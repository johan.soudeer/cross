package V1;

import java.util.Collections;
import java.util.Scanner;

public class Start {

	@SuppressWarnings("resource")
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		System.out.println("Choisir une lettre");
		String ch = scan.next();
		
		
		int taille = 0;
	
		while (taille<=0) {
		System.out.println("Quelle taille? (de 1 � l'infini)");
		taille = scan.nextInt();}

		int ratio = (ch.length()<2 ? 3:1);
		int lettre = ratio;
		int ext = 0;
		int mil = 2*(taille+ratio);
		int fin = mil+1;
		boolean retour = false;
		
		 while (mil < fin)
		{
			System.out.println(
			 String.join("", Collections.nCopies(ext, " "))
			+String.join("", Collections.nCopies(lettre, ch))
			+String.join("", Collections.nCopies(mil, " "))
			+String.join("", Collections.nCopies(lettre, ch)));


				if (mil>0 && retour == false)
				{
					ext+=1;
					mil-=2;	
				}
				else
				{
					retour=true;
					ext-=1;
					mil+=2;	
				}		
		}
	}
	
	
}
